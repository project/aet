<?php

namespace Drupal\aet;

use Drupal\Component\Uuid\Uuid;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\token\Token;
use Psr\Log\LoggerInterface;

/**
 * Class TokenReplacer.
 */
class TokenReplacer {

  /**
   * The token service.
   *
   * @var \Drupal\token\Token
   */
  protected $tokenService;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity display repository service.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The current-user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * This module's logging service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The options we're working with.
   *
   * @var array
   */
  protected $options;

  /**
   * The bubbleable metadata we're working with.
   *
   * @var \Drupal\Core\Render\BubbleableMetadata
   */
  protected $bubbleableMetadata;

  /**
   * TokenReplacer constructor.
   *
   * @param \Drupal\token\Token $token_service
   *   The token service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current-user service.
   * @param \Psr\Log\LoggerInterface $logger
   *   This module's logging service.
   * @param EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   */
  public function __construct(
    Token $token_service,
    EntityTypeManagerInterface $entity_type_manager,
    EntityDisplayRepositoryInterface $entity_display_repository,
    AccountProxyInterface $current_user,
    LoggerInterface $logger,
    EntityRepositoryInterface $entity_repository
  ) {
    $this->tokenService = $token_service;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->currentUser = $current_user;
    $this->logger = $logger;
    $this->entityRepository = $entity_repository;
  }

  /**
   * Associates the service with a set of options.
   *
   * It's necessary to call this method before any other non-static methods.
   *
   * @param array $options
   *   The options.
   *
   * @return $this
   *   The object itself, for method chaining.
   *
   * @see \Drupal\Core\Utility\Token::replace()
   */
  public function setOptions(array $options) {
    $this->options = $options + self::getDefaultOptions();
    return $this;
  }

  /**
   * Fetches the default options for token replacement.
   *
   * @return array
   *   The default options.
   *
   * @todo Add support for $options['callback', 'sanitize' and 'language']
   */
  public static function getDefaultOptions() {
    return [
      'callback' => NULL,
      'sanitize' => FALSE,
      'language' => \Drupal::languageManager()->getCurrentLanguage(),
      'clear' => FALSE,
    ];
  }

  /**
   * Fetches the options currently in use by the service.
   *
   * @return array
   *   The options.
   *
   * @throws \Exception
   *   If the data is missing.
   *
   * @see \Drupal\Core\Utility\Token::replace()
   */
  protected function getOptions() {
    if (!isset($this->options)) {
      throw new \Exception('This operation requires that the service be set with options.');
    }
    return $this->options;
  }

  /**
   * Associates the service with bubbleable metadata.
   *
   * It's necessary to call this method before any other non-static methods.
   *
   * @param \Drupal\Core\Render\BubbleableMetadata $bubbleable_metadata
   *   The bubbleable metadata.
   *
   * @return $this
   *   The object itself, for method chaining.
   *
   * @see \Drupal\Core\Utility\Token::replace()
   */
  public function setBubbleableMetadata(BubbleableMetadata $bubbleable_metadata) {
    $this->bubbleableMetadata = $bubbleable_metadata;
    return $this;
  }

  /**
   * Fetches the bubbleable metadata currently in use by the service.
   *
   * @return \Drupal\Core\Render\BubbleableMetadata
   *   The bubbleable metadata.
   *
   * @throws \Exception
   *   If the data is missing.
   *
   * @see \Drupal\Core\Utility\Token::replace()
   */
  protected function getBubbleableMetadata() {
    if (!isset($this->bubbleableMetadata)) {
      throw new \Exception('This operation requires that the service be set with options.');
    }
    return $this->bubbleableMetadata;
  }

  /**
   * Fetches replacements for the provided tokens.
   *
   * @param array $tokens
   *   The tokens.
   *
   * @return array
   *   The replacements.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getReplacements(array $tokens) {
    $replacements = [];

    // Checks all entities to see if they exist in the list of tokens.
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type_info) {
      $token_type = $entity_type_info->get('additional')['token_type'] ?: $entity_type_id;
      $tokens_for_type = $this->tokenService->findWithPrefix($tokens, $token_type);
      $replacements += $this->getReplacementsForTokenType($token_type, $tokens_for_type);
    }
    // Clears unreplaced tokens if the option is flagged.
    return $this->getReplacementsWithUnreplacedTokensClearedIfSet($tokens, $replacements);
  }

  /**
   * Fetches token replacements for specific token types.
   *
   * Each token type should map to an entity type ID, which is the second part
   * of the token.  e.g. "node" in [aet:node:1] and "file" in [aet:file:label].
   *
   * @param string $token_type
   *   The type of token, e.g. "node" or "file".
   * @param array $tokens
   *   The tokens to replace.
   *
   * @return array
   *   The replacements.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getReplacementsForTokenType(string $token_type, array $tokens) {
    list($entity_type_id, $entity_type_info) = $this->getEntityInfoMatchingTokenType($token_type);
    if (empty($entity_type_info)) {
      return [];
    }

    $replacements = [];
    foreach ($tokens as $key => $original) {
      if (
        (!$this->entityIdIsValid($entity_id = $this->getEntityIdFromTokenKey($key))) ||
        $this->recursiveTokenUseDetected($entity_type_id, $entity_type_info->getKey('id'), $entity_id)
      ) {
        continue;
      }

      if ($entity = $this->getEntityFromId($entity_type_id, $entity_id)) {
        $replacements += $this->getReplacementsForEntity($entity_type_id, $entity, $token_type, $tokens);
      }
    }

    return $this->getReplacementsWithUnreplacedTokensClearedIfSet($tokens, $replacements);
  }

  /**
   * Fetches an entity from its type and ID.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $entity_id
   *   The entity's ID.
   *
   * @return EntityInterface|null
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function getEntityFromId(string $entity_type_id, string $entity_id) {
    if (Uuid::isValid($entity_id)) {
      return $this->entityRepository->loadEntityByUuid($entity_type_id, $entity_id);
    }
    return $this->entityTypeManager->getStorage($entity_type_id)->load($entity_id);
  }

  /**
   * Determines if an entity ID is valid.
   *
   * @param string $entity_id
   *   The entity ID.
   *
   * @return bool
   *   TRUE if the entity is valid; FALSE otherwise.
   */
  protected function entityIdIsValid($entity_id) {
    return is_numeric($entity_id) || Uuid::isValid($entity_id);
  }

  /**
   * Determines if recursive token use has been detected.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $entity_type_key
   *   The entity type key (e.g. "nid" for nodes)
   * @param string $entity_id
   *   The entity's ID.
   *
   * @return bool
   *   TRUE if yes; FALSE otherwise.
   */
  protected function recursiveTokenUseDetected(string $entity_type_id, string $entity_type_key, $entity_id) {
    // Check if the entity to load is currently being viewed or if the
    // the rendered token origins from itself.
    if (
      $this->entityIsCurrentPage($entity_id, $entity_type_key) ||
      $this->entityIsBeingViewed($entity_type_id, $entity_type_key, $entity_id)
    ) {
      $this->logger->warning(t('Recursive token use detected.'));
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Checks if the current page is the full page view of the passed-in entity.
   *
   * @param int $entity_id
   *   An entity id.
   * @param string $id_key
   *   The entity id key (e.g. nid for nodes, fid for files).
   *
   * @return bool
   *   TRUE if yes; FALSE otherwise.
   */
  protected function entityIsCurrentPage($entity_id, $id_key) {

    // Returns the object of the entity being viewed.
    $page_entity = \Drupal::request()->attributes->get('node');

    // Checks if no entity is being viewed or if it doesn't contain the property
    // with $id_key as it's key.
    if (empty($page_entity) || !isset($page_entity->{$id_key})) {

      // If the above check is true, then the passed-in entity is not being
      // viewed directly.
      return FALSE;
    }

    // Returns true if the id of the passed in entity is equal to the id of the
    // entity being viewed.
    return $page_entity->{$id_key} === $entity_id;
  }

  /**
   * Checks if an entity is being viewed.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $entity_type_key
   *   The entity type key (e.g. "nid").
   * @param $entity_id
   *   The entity ID.
   *
   * @return bool
   *   TRUE if the entity with $id as its id and of type $entity_type is being
   *   viewed.
   */
  protected function entityIsBeingViewed(string $entity_type_id, string $entity_type_key, $entity_id) {

    // Get the function backtrace to check if the the entity view function is
    // part of the chain.
    $backtrace = debug_backtrace();
    foreach ($backtrace as $caller) {
      if ($caller['function'] === $entity_type_id . '_view') {

        // Move the $entity object to a new variable to increase readability.
        $entity = $caller['args'][0];

        // If the received ID matches the viewed entity ID (or UUID) then return
        // TRUE, which means the entity of the received ID is being viewed.
        if (($entity_id == $entity->{$entity_type_key}) || ($entity_id == $entity->uuid())) {
          return TRUE;
        }
      }
      elseif ($caller['function'] == '_drupal_bootstrap_full' || $caller == 'menu_execute_active_handler') {

        // There is no point in going back this far, so just stop.
        break;
      }
    }

    return FALSE;
  }

  /**
   * Fetches an entity ID from a token key.
   *
   * @param string $token_key
   *   The token key.
   *
   * @return string
   *   The entity ID.
   */
  protected function getEntityIdFromTokenKey(string $token_key) {
    // The position of the first colon is used for both checking if a second
    // part to the key exists and to separate between it (the first part) and
    // the rest of the key.
    $first_colon_pos = strpos($token_key, ':');

    // The $first_key is the part of the key until the first colon if exist,
    // or the full $token_key if no colon exists.
    return $first_colon_pos ? substr($token_key, 0, $first_colon_pos) : $token_key;
  }

  /**
   * Fetches token replacements for a specific entity.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $token_type
   *   The token type.
   * @param array $tokens
   *   The tokens.
   *
   * @return array
   *   The replacements.
   *
   * @throws \Exception
   */
  protected function getReplacementsForEntity(string $entity_type_id, EntityInterface $entity, string $token_type, array $tokens): array {
    if (!$entity->access('view', $this->currentUser)) {
      return [];
    }

    $replacements = $this->getWholeEntityReplacements($entity_type_id, $entity, $tokens);
    $replacements += $this->getEntityAttributeReplacements($entity, $token_type, $tokens);
    return $replacements;
  }

  /**
   * Fetches an entire entity for insertion (e.g. "[aet:node:999]").
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param EntityInterface $entity
   *   The entity.
   * @param array $tokens
   *   The list of tokens.
   *
   * @return array
   *   The list of replacements, with the entire entity included if available.
   */
  protected function getWholeEntityReplacements(string $entity_type_id, EntityInterface $entity, array $tokens) {
    $replacements = [];

    $has_id = isset($tokens[$entity->id()]);
    $has_uuid = isset($tokens[$entity->uuid()]);
    if (!$has_id && !$has_uuid) {
      return $replacements;
    }

    $available_view_modes = $this->entityDisplayRepository->getViewModes($entity_type_id);
    $rendered_entity = $this->getRenderedEntity(
      $entity_type_id,
      $entity,
      isset($available_view_modes['token']) ? 'token' : 'full'
    );

    if ($has_id) {
      $replacements[$tokens[$entity->id()]] = $rendered_entity;
    }

    if ($has_uuid) {
      $replacements[$tokens[$entity->uuid()]] = $rendered_entity;
    }

    return $replacements;
  }

  /**
   * Fetches replacements for entity attributes (e.g. "[aet:node:999:changed]").
   *
   * @param EntityInterface $entity
   *   The entity.
   * @param string $token_type
   *   The token type.
   * @param array $tokens
   *   The list of tokens.
   *
   * @return array
   *
   * @throws \Exception
   */
  protected function getEntityAttributeReplacements(EntityInterface $entity, string $token_type, array $tokens) {
    $replacements = [];

    $entity_tokens = $this->tokenService->findWithPrefix($tokens, $entity->id());
    $entity_tokens += $this->tokenService->findWithPrefix($tokens, $entity->uuid());
    $data = [
      $token_type => $entity,
    ];
    $replacements += $this->getEntityViewReplacements($token_type, $entity_tokens, $data);
    // For the remainder, after the entity type and ID have been figured out.
    $replacements += $this->tokenService->generate($token_type, $entity_tokens, $data, $this->getOptions(), $this->getBubbleableMetadata());
    return $replacements;
  }

  /**
   * Fetches the entity view replacements.
   *
   * @param string $token_type
   *   The type of token.
   * @param array $tokens
   *   The tokens.
   * @param array $data
   *   The data.
   *
   * @return array
   *   The token replacements.
   */
  protected function getEntityViewReplacements(string $token_type, array $tokens, array $data): array {
    $replacements = [];
    list($entity_type_id, $entity_type_info) = $this->getEntityInfoMatchingTokenType($token_type);

    if (!empty($entity_type_info) && !empty($data[$token_type])) {
      $entity = $data[$token_type];

      foreach ($this->entityDisplayRepository->getViewModes($entity_type_id) as $view_mode => $value) {
        if (empty($tokens['view-' . $view_mode])) {
          continue;
        }

        $replacements[$tokens['view-' . $view_mode]] = $this->getRenderedEntity($entity_type_id, $entity, $view_mode);
      }
    }

    // Returns the $replacements array.
    return $replacements;
  }

  /**
   * Fetches a rendered entity.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $view_mode
   *   The view mode.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The rendered HTML
   */
  protected function getRenderedEntity(string $entity_type_id, EntityInterface $entity, string $view_mode) {
    $renderable_entity = $this->entityTypeManager->getViewBuilder($entity_type_id)->view($entity, $view_mode);
    return render($renderable_entity);
  }

  /**
   * Fetches entity type information based on the type of token.
   *
   * @param string $token_type
   *   The token type.
   *
   * @return array
   *   The first item is the entity type ID, and the second one is the
   *   entity type instance, Drupal\Core\Entity\EntityType.
   */
  protected function getEntityInfoMatchingTokenType(string $token_type) {
    $entity_type_id = '';
    $entity_type_info = NULL;

    foreach ($this->entityTypeManager->getDefinitions() as $key => $value) {
      $entity_token_type = $value->get('additional')['token_type'] ?: NULL;

      if ($entity_token_type === $token_type) {
        $entity_type_id = $key;
        $entity_type_info = $value;
        break;
      }
    }

    return [$entity_type_id, $entity_type_info];
  }

  /**
   * Clears unreplaced tokens if the option was enabled.
   *
   * @param array $tokens
   *   The tokens.
   * @param array $replacements
   *   The replacements array.
   *
   * @return array
   *   The updated list of replacements.
   *
   * @throws \Exception
   */
  protected function getReplacementsWithUnreplacedTokensClearedIfSet(array $tokens, array $replacements): array {
    if (!$this->getOptions()['clear']) {
      return $replacements;
    }

    foreach ($tokens as $original) {
      if (!isset($replacements[$original])) {
        $replacements[$original] = '';
      }
    }

    return $replacements;
  }

}
