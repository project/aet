# Advanced Entity Tokens

## Table of Contents

* Introduction
* Dependencies
* Example use case
* Installing
* Maintainers

## Introduction

[Advanced Entity Tokens (AET)](https://www.drupal.org/project/aet) reveals all of Drupal's entities to the token system by using their IDs.

## Dependencies

* [Token](https://www.drupal.org/project/token)
* [Token Filter](https://www.drupal.org/project/token_filter) is not required, but it is recommended.

## Example use case

An example use case of what the module can provide is using taxonomy to manage your images and then using AET and Token Filter to output them in various text fields.

## Installing

1. Navigate to a release from [the list](https://www.drupal.org/project/aet/releases).
1. Run the *Install with Composer* command presented there.
1. Enable the AET module on your Drupal site.
1. Enter one of the above examples in a Token-supported field.  (Token Filter can be used to enable token filtering in general text fields.)

## Maintainers

* Original author & Drupal 7 maintainer: Eyal Shalev <eyalsh@gmail.com>
* Drupal 8 maintainer: [Colan Schwartz](https://www.drupal.org/u/colan) of [Consensus Enterprises](https://www.drupal.org/consensus-enterprises)

