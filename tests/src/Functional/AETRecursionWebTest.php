<?php

namespace Drupal\Tests\aet\Functional;

use Drupal\filter\Entity\FilterFormat;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests for Advanced Entity Tokens.
 *
 * Embedding a title or body of another node using token_filter works.
 * Embedding a full node into another one works.
 * Embedding the title into the body of the same node works,
 * but embedding the body inside the body is prevented.
 * A recursive loop alpha->beta->alpha is prevented.
 *
 * @group aet
 */
class AETRecursionWebTest extends BrowserTestBase {
  protected $defaultTheme = 'stark';
  public static $modules = [
    'node',
    'aet',
    'token_filter',
  ];

  protected $editor_user;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setUp() {
    parent::setUp();

    // Speed things up with a minimal profile.
    $this->profile = 'testing';

    // As using 'testing' profile, start from scratch.
    $this->drupalCreateContentType([
      'type' => 'page',
      'name' => 'Page',
    ]);

    // Setup permissions.
    $permissions = [
      'create page content',
      'edit any page content',
    ];
    $this->editor_user = $this->drupalCreateUser($permissions);

    FilterFormat::create([
      'format' => 'simple_tokens',
      'name' => 'Simple Tokens',
      'filters' => [
        'token_filter' => [
          'status' => 1,
        ],
      ],
    ])->save();
  }

  /**
   * Test normal operation - embed content from one node into another.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testSingleEmbedding() {
    $this->drupalLogin($this->editor_user);

    // Set up the target first.
    $embedded_text = 'I can be embedded.';
    $embedded_node_data = [
      'type' => 'page',
      'title' => 'Embed me',
      'body' => [
        [
          'value' => $embedded_text,
          'format' => 'simple_tokens',
        ],
      ],
    ];
    $embedded_node = $this->drupalCreateNode($embedded_node_data);

    // Build a node that embeds the target.
    $token = "[aet:node:{$embedded_node->id()}:body]";
    $embedder_node_data = [
      'type' => 'page',
      'title' => 'I contain an embed',
      'body' => [
        [
          'value' => "I am a wrapper around <blockquote>$token</blockquote>",
          'format' => 'simple_tokens',
        ],
      ],
    ];
    $embedder_node = $this->drupalCreateNode($embedder_node_data);
    $this->drupalGet("node/" . $embedder_node->id());
    // Content from the token pulling body data from the target was found in the
    // wrapper page when using token $token.
    $this->assertSession()->pageTextContains($embedded_text);
    // Using token $token did not trigger a recursive token warning.
    $this->assertSession()->pageTextNotContains("Recursive token use");

    // Test embedding full view of a node, not just a field.
    $token = "[aet:node:{$embedded_node->id()}]";
    $embedder_node->body = "I am embedding the whole node. <blockquote>$token</blockquote>";
    $embedder_node->save();
    $this->drupalGet("node/" . $embedder_node->id());
    // Using token $token did not trigger a recursive token warning.
    $this->assertSession()->pageTextNotContains("Recursive token use");
    // Embedded nodes title was found when embedding token $token.
    $this->assertSession()->pageTextContains($embedded_node_data['title']);
    // Embedded nodes body was found when embedding token $token.
    $this->assertSession()->pageTextContains($embedded_text);

    // Test embedding a user.
    $token = "[aet:user:{$this->editor_user->id()}]";
    $embedder_node->body = "I am embedding a display of my user. <blockquote>$token</blockquote>";
    $embedder_node->save();
    $this->drupalGet("node/" . $embedder_node->id());
    // Using token $token did not trigger a recursive token warning.
    $this->assertSession()->pageTextNotContains("Recursive token use");
    // User history name was found when embedding token $token.
    $this->assertSession()->pageTextContains("Member for");
  }

  /**
   * Test that it's OK to embed a different field from myself into myself.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testSelfEmbedding() {
    $this->drupalLogin($this->editor_user);

    $embedder_node_data = array(
      'type' => 'page',
      'title' => 'I contain an embed',
      'body' => [
        [
          'value' => "I will try to embed data from myself into my body",
          'format' => 'simple_tokens',
        ],
      ],
    );
    // Save once so I know what my nid is.
    $embedder_node = $this->drupalCreateNode($embedder_node_data);

    // I should be able to use a field from myself.
    $token = "[aet:node:{$embedder_node->id()}:title]";
    $embedder_node->body = "I am embedding a token from myself. My title is '$token'.";
    $embedder_node->save();

    $this->drupalGet("node/" . $embedder_node->id());
    // Node was able to embed its own title using a token $token.
    $this->assertSession()->pageTextContains("My title is '" . $embedder_node->label() . "'");
    // Using token $token did not trigger a recursive token warning.
    $this->assertSession()->pageTextNotContains("Recursive token use");

    // But if we tried to embed a token to the same field,
    // that would be illegal recursion.
    $token = "[aet:node:{$embedder_node->id()}:body]";
    $embedder_node->body = "I am embedding my own body into my body. <blockquote>$token</blockquote> This should fail.";
    $embedder_node->save();

    $this->drupalGet("node/" . $embedder_node->id());
    // Recursive token use was accurately detected.
    $this->assertSession()->pageTextContains("Recursive token use");
    // Attempting to embed the body inside itself failed correctly when using
    // token $token.
    $this->assertSession()->responseNotContains('<blockquote>I am embedding');
  }

  /**
   * Test that it's not OK to set up a cyclic inclusion pattern.
   *
   * Tries to embed alpha into the body of beta, and the beta into
   * the body of alpha. We expect this to be caught and stopped.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testCyclicEmbedding() {
    $this->drupalLogin($this->editor_user);

    $alpha_node_data = [
      'type' => 'page',
      'title' => 'I am the alpha',
      'body' => [
        [
          'value' => "I will try to embed data from the beta node into my body.",
          'format' => 'simple_tokens',
        ],
      ],
    ];
    // Save once so I know what my nid is.
    $alpha_node = $this->drupalCreateNode($alpha_node_data);

    $beta_node_data = [
      'type' => 'page',
      'title' => 'I am the beta',
      'body' => [
        [
          'value' => "This is the beta content. I will try to embed data from the alpha node named '[aet:node:{$alpha_node->id()}:title]' into my body. <blockquote>[aet:node:{$alpha_node->id()}:body]</blockquote>",
          'format' => 'simple_tokens',
        ],
      ],
    ];
    $beta_node = $this->drupalCreateNode($beta_node_data);

    // Update the alpha with a ref to the beta now we know its ID.
    $alpha_node->body = "This is the alpha content. I will try to embed data from the beta node named '[aet:node:{$beta_node->id()}:title]' into my body. <blockquote>[aet:node:{$beta_node->id()}:body]</blockquote>";
    $alpha_node->save();

    $this->drupalGet("node/" . $alpha_node->id());
    // This should still have been able to embed the TITLE of the beta,
    // But trying to embed the body should trigger a warning.
    // Alpha node should embed the beta nodes title OK.
    $this->assertSession()->pageTextContains("'I am the beta'");
    // Recursive token use was accurately detected when creating an embed loop.
    $this->assertSession()->pageTextContains("Recursive token use");
    // The beta content was not shown in the alpha body when there is recursion happening.
    $this->assertSession()->pageTextNotContains("This is the beta content");
  }

}
